package br.edu.unisep.benefits.domain.dto;

import br.edu.unisep.benefits.domain.dto.user.UserDto;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class LoginResultDto {

    private final UserDto userData;

    private final String token;

}
