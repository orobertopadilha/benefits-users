package br.edu.unisep.benefits.domain.usecase.user;

import br.edu.unisep.benefits.data.repository.UserRepository;
import br.edu.unisep.benefits.domain.dto.user.UserDto;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class GetUserByIdUseCase {

    private final UserRepository repository;

    public UserDto execute(Integer id) {
        var user = repository.findById(id).get();
        return new UserDto(user.getId(), user.getLogin(), user.getName(), null);
    }

}
