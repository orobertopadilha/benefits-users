package br.edu.unisep.benefits;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BenefitsUsersApplication {

    public static void main(String[] args) {
        SpringApplication.run(BenefitsUsersApplication.class, args);
    }

}
