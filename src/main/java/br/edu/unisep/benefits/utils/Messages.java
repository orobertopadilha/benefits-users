package br.edu.unisep.benefits.utils;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class Messages {

    public static final String MESSAGE_INVALID_LOGIN = "Dados inválidos para o login!";

}
